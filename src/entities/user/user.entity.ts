import { CommonEntity } from '@anpham1925/nestjs/dist/entities/common';
import { Asset } from 'src/entities/asset/asset.entity';
import { Column, DeleteDateColumn, Entity, JoinColumn } from 'typeorm';
import { USER_ROLE } from './enums/userRole.enum';

@Entity()
export class User extends CommonEntity {
  @Column({
    unique: true,
  })
  email: string;

  @Column({
    default: false,
  })
  isVerified: boolean;

  @Column({
    default: false,
  })
  isBanned: boolean;

  @Column({
    default: new Date(),
  })
  passwordChangedAt: Date;

  @Column({
    nullable: true,
    length: 60,
  })
  password: string;

  @Column({
    nullable: true,
  })
  fullName: string;

  //   @OneToMany(() => Conversation, (conversation) => conversation.userOneId)
  //   conversationUserOne: Conversation[];

  //   @OneToMany(() => Conversation, (conversation) => conversation.userTwoId)
  //   conversationUserTwo: Conversation[];

  //   @OneToMany(() => Message, (message) => message.senderId)
  //   messageSender: Message[];

  //   @OneToMany(
  //     () => ConversationUser,
  //     (conversationUser) => conversationUser.user,
  //   )
  //   conversationUser: ConversationUser[];

  @DeleteDateColumn()
  deletedAt: Date;

  @JoinColumn()
  avatar: Asset;

  @Column({
    type: 'enum',
    enum: USER_ROLE,
    default: USER_ROLE.NORMAL,
  })
  role: USER_ROLE;

  @Column({ default: 0 })
  countBadwords: number;

  @Column({
    default: false,
  })
  isBadwordFiltered: boolean;
}
