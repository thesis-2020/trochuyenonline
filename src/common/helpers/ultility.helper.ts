import { getError } from '@anpham1925/nestjs';
import { HttpStatus } from '@nestjs/common';
import { Request } from 'express';
import * as mimeTypes from 'mime-types';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';

export const getObjectFromEnum = (
  customEnum: Record<string, any>,
): Record<string, unknown> => {
  const result: Record<string, any> = {};
  const keys = getKeyNameFromEnum(customEnum);
  keys.map((key) => {
    result[key] = customEnum[key];
  });

  return result;
};

export const getKeyNameFromEnum = (
  customEnum: Record<string, unknown>,
): string[] => {
  return Object.keys(customEnum).filter((key) => !(parseInt(key) >= 0));
};

export const imageFileFilter = (req: any, file: any, callback: any): any => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(
      getError(
        'Only images and pdf are allowed (jpg, jpeg, png, gif)',
        HttpStatus.BAD_REQUEST,
      ),
    );
  }
  callback(null, true);
};

export const editFileName = (
  req: Request,
  file: Express.Multer.File,
  callback: (a, b, c?) => void,
): void => {
  const extension = mimeTypes.extension(file.mimetype);

  const randomName = uuidv4();
  callback(null, `${randomName}.${extension}`);
};

export const multerOptions = {
  fileFilter: imageFileFilter,
  limits: { fileSize: 3000000 },
  storage: diskStorage({
    destination: './uploads',
    filename: editFileName,
  }),
};
