import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto-js';

@Injectable()
export class KeyHelper {
  secret = '';
  constructor(private readonly configService: ConfigService) {
    this.secret = this.configService.get('KEY_SECRET');
  }

  createKey() {
    const salt = crypto.lib.WordArray.random(128 / 8);
    return crypto
      .PBKDF2(this.secret, salt, {
        keySize: 128 / 32,
      })
      .toString();
  }
}
