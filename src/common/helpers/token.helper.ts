import { HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as jwt from 'jsonwebtoken';
import { TOKEN_TYPE } from '../constants/admin/tokenType.constant';
import { customThrowError } from '@anpham1925/nestjs';

@Injectable()
export class TokenHelper {
  secret = '';

  constructor(private readonly configService: ConfigService) {
    this.secret = this.configService.get('TOKEN_SECRET');
  }

  createToken(data: Record<string, unknown>): any {
    try {
      const token = jwt.sign(
        { ...data, iat: Math.floor(Date.now() / 1000) },
        this.secret,
      );
      return token;
    } catch (error) {
      customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
    }
  }

  verifyToken(token: string, type: string = TOKEN_TYPE.LOGIN): any {
    try {
      const data: any = jwt.verify(token, this.secret);
      if (data.type === type) return data;
      customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
    } catch (error) {
      customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
    }
  }
}
