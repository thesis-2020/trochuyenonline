import { customThrowError, METADATA } from '@anpham1925/nestjs';
import {
  CanActivate,
  ExecutionContext,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { Observable } from 'rxjs';
import { USER_ROLE } from 'src/entities/user/enums/userRole.enum';
import { User } from 'src/entities/user/user.entity';
import { UserService } from 'src/modules/default/user/user.service';
import { FindOneOptions, LessThanOrEqual } from 'typeorm';
import { CustomRequest } from '../interfaces/CustomRequest.interface';

@Injectable()
export class RoleAuthenticationGuard implements CanActivate {
  constructor(
    private reflector: Reflector,

    private readonly userService: UserService, // @InjectRepository(UserRepository) // private readonly userRepository: UserRepository,
  ) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const isPublic = this.reflector.get<boolean>(
      METADATA.IS_PUBLIC,
      context.getHandler(),
    );

    const allowRoles = this.reflector.get<USER_ROLE[]>(
      METADATA.ROLE,
      context.getClass(),
    );

    if (isPublic) {
      return true;
    }

    const request = this._getRequest(context);
    return this._handleRequest(request, allowRoles);
  }

  private async _handleRequest(
    req: Request,
    allowRoles: USER_ROLE[],
  ): Promise<boolean> {
    const token = (req as any).user;

    if (!token) {
      this._throwPermissionDenied();
    }

    const options: FindOneOptions<User> = {
      where: {
        email: token.email,
        passwordChangedAt: LessThanOrEqual(new Date(token.iat * 1000)),
      },
      select: ['id', 'role', 'email'],
    };

    const user = await this.userService.getAuthInstance(options);

    if (!user) {
      this._throwPermissionDenied();
    }

    if (!allowRoles.includes(user.role)) {
      this._throwPermissionDenied();
    }

    (req as CustomRequest).authInstance = user;

    return true;
  }

  private _getRequest<T = any>(context: ExecutionContext): T {
    return context.switchToHttp().getRequest();
  }

  private _throwPermissionDenied(): void {
    customThrowError('Permission denied', HttpStatus.FORBIDDEN);
  }
}
