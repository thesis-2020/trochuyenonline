import { Request } from 'express';
import { User } from 'src/entities/user/user.entity';
export interface CustomRequest extends Request {
  authInstance: User;
}
