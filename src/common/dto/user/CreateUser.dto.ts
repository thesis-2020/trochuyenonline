import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty } from 'class-validator';
import { Transform } from 'class-transformer';
import { USER_ROLE } from 'src/entities/user/enums/userRole.enum';
import { getObjectFromEnum } from 'src/common/helpers/ultility.helper';

export class CreateUserDto {
  @IsEmail()
  @ApiProperty()
  @Transform((value: string) => value.toLowerCase().trim())
  email: string;

  @IsNotEmpty()
  @ApiProperty()
  @Transform((value: string) => value.trim())
  fullName?: string;

  @IsNotEmpty()
  @ApiProperty()
  password: string;
}

export class AdminCreateUserDto extends CreateUserDto {
  @IsEnum(USER_ROLE)
  @ApiProperty({
    enum: [USER_ROLE.NORMAL, USER_ROLE.ADMIN],
    description: JSON.stringify(getObjectFromEnum(USER_ROLE)),
  })
  role: USER_ROLE;
}

export class UpdateUserDto {
  @IsNotEmpty()
  @ApiProperty()
  @Transform((value: string) => value.trim())
  fullName?: string;
}

export class AdminUpdateUserDto extends UpdateUserDto {
  @IsEmail()
  @ApiProperty()
  @Transform((value: string) => value.toLowerCase().trim())
  email: string;

  @IsEnum(USER_ROLE)
  @ApiProperty({
    enum: [USER_ROLE.NORMAL, USER_ROLE.ADMIN],
    description: JSON.stringify(getObjectFromEnum(USER_ROLE)),
  })
  role: USER_ROLE;
}
