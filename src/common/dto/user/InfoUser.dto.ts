import { Exclude, Expose } from 'class-transformer';
import { USER_ROLE } from 'src/entities/user/enums/userRole.enum';
import { Asset } from 'src/entities/asset/asset.entity';

export class InfoUserDto {
  token: string;
  email: string;
  role: USER_ROLE;
  fullName: string;
  createdAt: Date;
  passwordChangedAt: Date;

  @Exclude()
  password: string;

  @Exclude()
  avatar: Asset;

  @Expose({
    name: 'avatarUrl',
  })
  get avatarUrl(): string {
    return this.avatar
      ? `${process.env.BACKEND_HOST}/api/assets/${this.avatar.id}.${this.avatar.extension}`
      : '';
  }

  constructor(partial: Partial<InfoUserDto>) {
    Object.assign(this, partial);
  }
}
