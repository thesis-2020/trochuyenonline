import { customThrowError } from '@anpham1925/nestjs';
import { HttpStatus } from '@nestjs/common';
import { Asset } from 'src/entities/asset/asset.entity';
import { ASSET_REFERENCE_TYPE } from 'src/entities/asset/enums/assetReferenceType.enum';
import { USER_ROLE } from 'src/entities/user/enums/userRole.enum';
import { User } from 'src/entities/user/user.entity';
import { EntityRepository, FindManyOptions, Repository } from 'typeorm';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async _checkExistedUser(email: string) {
    const existed = await this.findOne({ email });

    if (existed) {
      customThrowError(
        'This email address is already in use',
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async _updateAccount(id: number, model: any, role?: USER_ROLE) {
    const user = await this.findOne(id);
    user.fullName = model.fullName;
    if (model.role) {
      user.role = model.role;
    }
    if (model.email) {
      // superadmin has the right for edit anything
      // admin just has the right for edit user info
      // and yes! he can not edit his own email even if he is an admin. In that case, he will use change email tab
      if (role !== USER_ROLE.SUPER_ADMIN && user.role !== USER_ROLE.NORMAL) {
        customThrowError('Invalid Credential', HttpStatus.BAD_REQUEST);
      }

      user.email = model.email;
    }
    await this.save(user);
    return true;
  }

  async getLoginUserWithOptions(findOptions): Promise<User> {
    const user = await this.createQueryBuilder('u')
      .where('u.email = :email', { ...findOptions })
      .leftJoinAndMapOne(
        'u.avatar',
        Asset,
        'a',
        'a.referenceId = u.id  and a.referenceType = :type',
        { type: ASSET_REFERENCE_TYPE.USER_IMG },
      )
      .getOne();

    if (!user) {
      customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
    }

    if (!user.isVerified) {
      customThrowError(
        'Please verify your account before login!',
        HttpStatus.UNAUTHORIZED,
      );
    }

    return user;
  }

  async search(options: FindManyOptions<User>): Promise<[User[], number]> {
    const { where, skip, take, order } = options;

    const query = this.createQueryBuilder('u')
      .where(where)
      .leftJoinAndMapOne(
        'u.avatar',
        Asset,
        'a',
        'a.referenceId = u.id  and a.referenceType = :type',
        { type: ASSET_REFERENCE_TYPE.USER_IMG },
      );

    const orderKeys = Object.keys(order || {});

    if (orderKeys.length) {
      query.orderBy(orderKeys[0], order[orderKeys[0]]);
    } else {
      query.orderBy('u.createdAt', 'DESC');
    }

    const users = await query.skip(skip).take(take).getManyAndCount();
    return users;
  }

  async getUser(userId: number): Promise<User> {
    const user = await this.createQueryBuilder('u')
      .where('u.id = :id', { id: userId })
      .leftJoinAndMapOne(
        'u.avatar',
        Asset,
        'a',
        'a.referenceId = u.id  and a.referenceType = :type',
        { type: ASSET_REFERENCE_TYPE.USER_IMG },
      )
      .getOne();

    return user;
  }

  async getListUsers(
    options: FindManyOptions<User>,
  ): Promise<[User[], number]> {
    const { where, skip, take, order, withDeleted } = options;

    const query = this.createQueryBuilder('u')
      .where(where)
      .leftJoinAndMapOne(
        'u.avatar',
        Asset,
        'a',
        'a.referenceId = u.id and a.referenceType = :type',
        { type: ASSET_REFERENCE_TYPE.USER_IMG },
      );

    const orderKeys = Object.keys(order || {});

    if (withDeleted) {
      query.withDeleted();
    }

    if (orderKeys.length) {
      query.orderBy(orderKeys[0], order[orderKeys[0]]);
    } else {
      query.orderBy('u.createdAt', 'DESC');
    }

    const users = await query.skip(skip).take(take).getManyAndCount();

    return users;
  }
}
