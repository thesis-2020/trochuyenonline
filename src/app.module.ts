import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TokenHelper } from './common/helpers/token.helper';
import { AuthenticateMiddleware } from './common/middlewares/authentication.middleware';
import { adminModules, defaultModules } from './modules/export.module';
import { Log } from '@anpham1925/nestjs';
import { User } from './entities/user/user.entity';
import { Asset } from './entities/asset/asset.entity';
import { UserRepository } from './repositories/user.repository';

const env = process.env.NODE_ENV || 'development';

const envFilePath =
  env === 'development' ? '.env' : `.env${process.env.NODE_ENV}`;

const entities = [Log, User, Asset];

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('DB_HOST'),
        port: +configService.get<number>('DB_PORT'),
        username: configService.get('DB_USER'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_NAME'),
        entities,
        synchronize: true,
        logging: false,
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([UserRepository]),
    ScheduleModule.forRoot(),
    ...adminModules,
    ...defaultModules,
  ],
  controllers: [AppController],
  providers: [AppService, TokenHelper],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(AuthenticateMiddleware).forRoutes('*');
  }
}
