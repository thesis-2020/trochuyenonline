import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import * as express from 'express';
import * as helmet from 'helmet';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { adminModules, defaultModules } from './modules/export.module';
import {
  AllExceptionsFilter,
  FormatResponseInterceptor,
  LogModule,
  LogService,
} from '@anpham1925/nestjs';
import { join } from 'path';
import { RoleAuthenticationGuard } from './common/guards/role.guard';
import { UserModule } from './modules/default/user/user.module';
import { UserService } from './modules/default/user/user.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');
  app.enableCors();
  app.use(helmet());

  const configService = app.get(ConfigService);
  const port = configService.get('APP_PORT');

  const options = new DocumentBuilder()
    .setTitle('trochuyenonline APIs')
    .setDescription('Detail of trochuyenonline APIs')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options, {
    include: [...adminModules, ...defaultModules],
    deepScanRoutes: true,
    operationIdFactory: (key: string, method: string) => method,
  });

  SwaggerModule.setup('api/swagger', app, document);

  const userService = app.select(UserModule).get(UserService);

  const logService = app.select(LogModule).get(LogService);

  app.useGlobalFilters(new AllExceptionsFilter(logService));

  app.useGlobalInterceptors(new FormatResponseInterceptor());

  app.use('/images', express.static(join(__dirname, 'images')));

  app.useGlobalPipes(
    new ValidationPipe({
      forbidUnknownValues: false,
      skipMissingProperties: true,
      transform: true,
    }),
  );

  app.useGlobalGuards(
    new RoleAuthenticationGuard(new Reflector(), userService),
  );

  await app.listen(port, '0.0.0.0');

  console.log(`api app is working on port: ${port}`);
}
bootstrap();
