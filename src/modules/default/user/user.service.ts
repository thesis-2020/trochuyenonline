import {
  customThrowError,
  PaginationRequest,
  PaginationResult,
} from '@anpham1925/nestjs';
import { HttpStatus, Injectable } from '@nestjs/common';
import { TOKEN_TYPE } from 'src/common/constants/admin/tokenType.constant';
import { UploadFile } from 'src/common/dto/asset/UploadFile.dto';
import {
  CreateUserDto,
  UpdateUserDto,
} from 'src/common/dto/user/CreateUser.dto';
import { InfoUserDto } from 'src/common/dto/user/InfoUser.dto';
import { KeyHelper } from 'src/common/helpers/key.helper';
import { PasswordHelper } from 'src/common/helpers/password.helper';
import { TokenHelper } from 'src/common/helpers/token.helper';
import { ASSET_REFERENCE_TYPE } from 'src/entities/asset/enums/assetReferenceType.enum';
import { CREATE_USER_SOURCE } from 'src/entities/user/enums/createUserSource.enum';
import { User } from 'src/entities/user/user.entity';
import { AssetRepository } from 'src/repositories/asset.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { FindManyOptions, FindOneOptions, Not, Raw } from 'typeorm';
import { LoginUserDto } from './dto/LoginUser.dto';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly assetRepository: AssetRepository,
    //   private readonly conversationRepository: ConversationRepository,
    private readonly passwordHelper: PasswordHelper,
    private readonly tokenHelper: TokenHelper,
    private readonly keyHelper: KeyHelper,
  ) {}

  private async _createAccount(model: any, source: CREATE_USER_SOURCE) {
    const hash = await this.passwordHelper.createHash(model.password);
    const user = new User();
    user.email = model.email;
    user.fullName = model.fullName;
    user.password = hash;
    user.isVerified = source != CREATE_USER_SOURCE.REGISTRATION;
    user.role = model.role;

    await this.userRepository.save(user);

    // const token = this.tokenHelper.createToken({
    //   id: user.id,
    //   email: user.email,
    //   type: TOKEN_TYPE.REGISTER,
    // });

    //   this.mailHelper.sendVerifyEmail(token, user.email);

    return true;
  }

  private async _checkPassword(plain: string, hash: string) {
    const matched = await this.passwordHelper.checkHash(plain, hash);

    if (!matched) {
      customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
    }
  }

  async verifyToken(token: string): Promise<InfoUserDto> {
    const data = this.tokenHelper.verifyToken(token);

    const user = await this.userRepository.getLoginUserWithOptions({
      email: data.email,
    });

    if (!user) {
      customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
    }

    return new InfoUserDto({ ...user, token });
  }

  //   async forgotPassword(model: ForgotPasswordDto): Promise<boolean> {
  //     const user = await this.userRepository.findOne({ email: model.email });

  //     if (!user) {
  //       customThrowError('Email not found!', HttpStatus.NOT_FOUND);
  //     }

  //     const token = this.tokenHelper.createToken({
  //       id: user.id,
  //       email: user.email,
  //       type: TOKEN_TYPE.FORGOT_PASSWORD,
  //     });

  //     this.mailHelper.sendForgotPassword(token, user.email);

  //     return true;
  //   }

  // async resetPassword(model: ResetPasswordDto): Promise<boolean> {
  //   const now = Math.floor(Date.now() / 1000) * 1000;

  //   const data = this.tokenHelper.verifyToken(
  //     model.token,
  //     TOKEN_TYPE.FORGOT_PASSWORD,
  //   );

  //   const user = await this.userRepository.findOne({ email: data.email });

  //   if (!user) {
  //     customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
  //   }

  //   user.password = await this.passwordHelper.createHash(model.password);
  //   user.passwordChangedAt = new Date(now);

  //   await this.userRepository.save(user);

  //   this.mailHelper.sendConfirmChangedPassword(user.email);

  //   return true;
  // }

  // async changePassword(
  //   model: ChangePasswordDto,
  //   userId: number,
  // ): Promise<boolean> {
  //   if (model.newPassword === model.oldPassword) {
  //     customThrowError(
  //       'Your new password is the same as the old one!',
  //       HttpStatus.CONFLICT,
  //     );
  //   }

  //   const now = Math.floor(Date.now() / 1000) * 1000;

  //   const user = await this.userRepository.findOne({ id: userId });

  //   if (!user) {
  //     customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
  //   }

  //   await this._checkPassword(model.oldPassword, user.password);

  //   user.password = await this.passwordHelper.createHash(model.newPassword);
  //   user.passwordChangedAt = new Date(now);

  //   await this.userRepository.save(user);

  //   this.mailHelper.sendConfirmChangedPassword(user.email);

  //   return true;
  // }

  async verifyEmail(token: string): Promise<InfoUserDto> {
    const data = this.tokenHelper.verifyToken(token, TOKEN_TYPE.REGISTER);

    const user = await this.userRepository.findOne({ email: data.email });

    if (!user) {
      customThrowError('Invalid Credential', HttpStatus.UNAUTHORIZED);
    }

    user.isVerified = true;
    await this.userRepository.save(user);

    const loginToken = this.tokenHelper.createToken({
      id: user.id,
      email: user.email,
      type: TOKEN_TYPE.LOGIN,
    });

    const result = {
      ...user,
      token: loginToken,
    };

    return new InfoUserDto(result);
  }

  async registerUser(
    model: CreateUserDto,
    source: CREATE_USER_SOURCE,
  ): Promise<boolean> {
    await this.userRepository._checkExistedUser(model.email);
    await this._createAccount(model, source);

    return true;
  }

  async login(model: LoginUserDto): Promise<InfoUserDto> {
    const user = await this.userRepository.getLoginUserWithOptions({
      email: model.email,
    });

    await this._checkPassword(model.password, user.password);

    const token = this.tokenHelper.createToken({
      id: user.id,
      email: user.email,
      type: TOKEN_TYPE.LOGIN,
      role: user.role,
    });

    return new InfoUserDto({
      token,
      ...user,
    });
  }

  async uploadAvatar(
    model: UploadFile,
    file: Express.Multer.File,
    userId: number,
  ): Promise<string> {
    const avatar = await this.assetRepository.findOne({
      where: {
        referenceId: userId,
        referenceType: ASSET_REFERENCE_TYPE.USER_IMG,
      },
    });

    if (avatar) {
      await this.assetRepository.delete({ id: avatar.id });
    }

    await this.assetRepository.saveAsset(
      file,
      ASSET_REFERENCE_TYPE.USER_IMG,
      userId,
    );

    return `${process.env.BACKEND_HOST}/api/assets/${file.filename}`;
  }

  async updateInfo(model: UpdateUserDto, userId: number): Promise<boolean> {
    await this.userRepository._updateAccount(userId, model);
    return true;
  }

  async search(
    query: PaginationRequest<User>,
    userId: number,
  ): Promise<PaginationResult<InfoUserDto>> {
    const { skip, take, search, orderBy, orderDirection } = query;

    const options: FindManyOptions<User> = {
      skip,
      take,
    };

    const searchMethod = Raw(
      (alias) => `LOWER(${alias}) like '%${search.toLowerCase()}%'`,
    );

    options.where = { id: Not(userId) };

    if (search) {
      options.where = [
        { ...options.where, email: searchMethod },
        { ...options.where, firstName: searchMethod },
        { ...options.where, lastName: searchMethod },
      ];
    }

    if (orderBy) {
      options.order = {
        [orderBy]: orderDirection,
      };
    }

    const [data, count] = await this.userRepository.search(options);

    return new PaginationResult<InfoUserDto>(
      data.map((u) => new InfoUserDto(u)),
      count,
    );
  }

  async getAuthInstance(options: FindOneOptions<User>): Promise<User> {
    return await this.userRepository.findOne(options);
  }
}
