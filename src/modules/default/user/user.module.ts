import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PasswordHelper } from 'src/common/helpers/password.helper';
import { TokenHelper } from 'src/common/helpers/token.helper';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { KeyHelper } from 'src/common/helpers/key.helper';
import { AssetRepository } from 'src/repositories/asset.repository';
import { UserRepository } from 'src/repositories/user.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      AssetRepository,
      //   ConversationRepository,
    ]),
  ],
  controllers: [UserController],
  providers: [UserService, PasswordHelper, TokenHelper, KeyHelper],
})
export class UserModule {}
