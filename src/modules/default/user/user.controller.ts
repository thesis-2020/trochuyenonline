import { METADATA } from '@anpham1925/nestjs';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Post,
  Req,
  SetMetadata,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { MODULE } from 'src/common/constants/modules/module.constant';
import { UploadImage } from 'src/common/dto/asset/UploadImage.dto';
import { CreateUserDto } from 'src/common/dto/user/CreateUser.dto';
import { InfoUserDto } from 'src/common/dto/user/InfoUser.dto';
import { multerOptions } from 'src/common/helpers/ultility.helper';
import { CREATE_USER_SOURCE } from 'src/entities/user/enums/createUserSource.enum';
import { USER_ROLE } from 'src/entities/user/enums/userRole.enum';
import { CheckToken } from './dto/CheckToken.dto';
import { LoginUserDto } from './dto/LoginUser.dto';
import { TokenBody } from './dto/TokenBody.dto';
import { UserService } from './user.service';

@ApiTags('User')
@ApiBearerAuth()
@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
@SetMetadata(METADATA.ROLE, [
  USER_ROLE.NORMAL,
  USER_ROLE.ADMIN,
  USER_ROLE.SUPER_ADMIN,
])
@SetMetadata(METADATA.MODULE, MODULE.USER)
export class UserController {
  constructor(private readonly userService: UserService) {}

  /**
   * Account
   */

  @SetMetadata(METADATA.IS_PUBLIC, true)
  @Post()
  registerUser(@Body() createUserModel: CreateUserDto): Promise<boolean> {
    return this.userService.registerUser(
      createUserModel,
      CREATE_USER_SOURCE.REGISTRATION,
    );
  }

  @SetMetadata(METADATA.IS_PUBLIC, true)
  @Post('login')
  login(@Body() model: LoginUserDto): Promise<InfoUserDto> {
    return this.userService.login(model);
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'User avatar body',
    type: UploadImage,
  })
  @UseInterceptors(FileInterceptor('image', multerOptions))
  @Post('upload-avatar')
  create(
    @Body() model: UploadImage,
    @UploadedFile() file: Express.Multer.File,
    @Req() request: Request,
  ): Promise<string> {
    return this.userService.uploadAvatar(model, file, (request as any).user.id);
  }

  @SetMetadata(METADATA.IS_PUBLIC, true)
  @ApiBearerAuth()
  @Post('check-token')
  verifyToken(@Body() model: CheckToken): Promise<InfoUserDto> {
    return this.userService.verifyToken(model.token);
  }

  // @SetMetadata(METADATA.IS_PUBLIC, true)
  // @SetMetadata(METADATA.ACTION, USER_ACCOUNT_ACTION.USER_FOGOT_PASSWORD)
  // @Post('forgot-password')
  // forgotPassword(@Body() model: ForgotPasswordDto): Promise<boolean> {
  //   return this.userService.forgotPassword(model);
  // }

  // @SetMetadata(METADATA.IS_PUBLIC, true)
  // @SetMetadata(METADATA.ACTION, USER_ACCOUNT_ACTION.USER_RESET_PASSWORD)
  // @Post('reset-password')
  // resetPassword(@Body() model: ResetPasswordDto): Promise<boolean> {
  //   return this.userService.resetPassword(model);
  // }

  // @Post('change-password')
  // @SetMetadata(METADATA.ACTION, USER_ACCOUNT_ACTION.USER_CHANGE_PASSWORD)
  // changePassword(
  //   @Body() model: ChangePasswordDto,
  //   @Req() request: Request,
  // ): Promise<boolean> {
  //   return this.userService.changePassword(model, (request as any).user.id);
  // }

  @SetMetadata(METADATA.IS_PUBLIC, true)
  @Post('verify-email')
  verifyEmail(@Body() model: TokenBody): Promise<InfoUserDto> {
    return this.userService.verifyEmail(model.token);
  }
}
