import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class TokenBody {
  @ApiProperty()
  @IsNotEmpty()
  token: string;
}
