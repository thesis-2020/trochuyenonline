import { LogModule } from '@anpham1925/nestjs';
import { UserModule } from './default/user/user.module';

export const adminModules = [];

export const defaultModules = [UserModule, LogModule];
